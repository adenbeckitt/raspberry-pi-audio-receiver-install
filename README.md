This is a general fix from ehsmaes' version. This now works with Raspbian Jessie.

I have made many more alterations (a fair bit of it is brand new) that I will be sharing on GitHub at some point. Until then, this should work for you.